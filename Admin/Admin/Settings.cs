﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Admin
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void send_Click(object sender, EventArgs e)
        {
            if (ipTextBox.Text.Trim() != "")
            {
                try
                {

                    DirectoryInfo data = new DirectoryInfo(@"ClientInfo");
                    data.Create();

                    var sw = new StreamWriter(@"ClientInfo/data_info.txt");
                    sw.WriteLine(ipTextBox.Text);
                    sw.Close();

                    this.Hide();

                    Application.Restart();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка: " + ex.Message);
                }
            }
        }
    }
}
