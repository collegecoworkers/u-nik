﻿namespace Admin
{
    partial class NewItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.ni_name = new System.Windows.Forms.TextBox();
			this.ni_model_v = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.ni_model_proc = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.ni_num_kern = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.ni_mem_v = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.ni_rate_kern = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.ni_ram = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.ni_rom = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.send_ni = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(12, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Название";
			// 
			// ni_name
			// 
			this.ni_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ni_name.Location = new System.Drawing.Point(15, 37);
			this.ni_name.Name = "ni_name";
			this.ni_name.Size = new System.Drawing.Size(327, 23);
			this.ni_name.TabIndex = 1;
			// 
			// ni_model_v
			// 
			this.ni_model_v.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ni_model_v.Location = new System.Drawing.Point(15, 100);
			this.ni_model_v.Name = "ni_model_v";
			this.ni_model_v.Size = new System.Drawing.Size(327, 23);
			this.ni_model_v.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(12, 75);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(233, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "Модель видеокарты (NVIDIA/AMD)";
			// 
			// ni_model_proc
			// 
			this.ni_model_proc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ni_model_proc.Location = new System.Drawing.Point(15, 222);
			this.ni_model_proc.Name = "ni_model_proc";
			this.ni_model_proc.Size = new System.Drawing.Size(330, 23);
			this.ni_model_proc.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(12, 197);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(214, 17);
			this.label3.TabIndex = 4;
			this.label3.Text = "Модель процессора (Intel/AMD)";
			// 
			// ni_num_kern
			// 
			this.ni_num_kern.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ni_num_kern.Location = new System.Drawing.Point(15, 285);
			this.ni_num_kern.Name = "ni_num_kern";
			this.ni_num_kern.Size = new System.Drawing.Size(327, 23);
			this.ni_num_kern.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(12, 260);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(122, 17);
			this.label4.TabIndex = 6;
			this.label4.Text = "Количество ядер";
			// 
			// ni_mem_v
			// 
			this.ni_mem_v.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ni_mem_v.Location = new System.Drawing.Point(15, 161);
			this.ni_mem_v.Name = "ni_mem_v";
			this.ni_mem_v.Size = new System.Drawing.Size(327, 23);
			this.ni_mem_v.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(12, 136);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(174, 17);
			this.label5.TabIndex = 8;
			this.label5.Text = "Память видеокарты (MB)";
			// 
			// ni_rate_kern
			// 
			this.ni_rate_kern.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ni_rate_kern.Location = new System.Drawing.Point(15, 344);
			this.ni_rate_kern.Name = "ni_rate_kern";
			this.ni_rate_kern.Size = new System.Drawing.Size(327, 23);
			this.ni_rate_kern.TabIndex = 6;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label6.Location = new System.Drawing.Point(12, 319);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(99, 17);
			this.label6.TabIndex = 10;
			this.label6.Text = "Частота ядра";
			// 
			// ni_ram
			// 
			this.ni_ram.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ni_ram.Location = new System.Drawing.Point(15, 404);
			this.ni_ram.Name = "ni_ram";
			this.ni_ram.Size = new System.Drawing.Size(327, 23);
			this.ni_ram.TabIndex = 7;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label7.Location = new System.Drawing.Point(12, 379);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(182, 17);
			this.label7.TabIndex = 12;
			this.label7.Text = "Оперативная память (МБ)";
			// 
			// ni_rom
			// 
			this.ni_rom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ni_rom.Location = new System.Drawing.Point(15, 464);
			this.ni_rom.Name = "ni_rom";
			this.ni_rom.Size = new System.Drawing.Size(327, 23);
			this.ni_rom.TabIndex = 8;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label8.Location = new System.Drawing.Point(12, 439);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(202, 17);
			this.label8.TabIndex = 14;
			this.label8.Text = "Свободное место на ЖД (ГБ)";
			// 
			// send_ni
			// 
			this.send_ni.Location = new System.Drawing.Point(101, 506);
			this.send_ni.Name = "send_ni";
			this.send_ni.Size = new System.Drawing.Size(150, 41);
			this.send_ni.TabIndex = 16;
			this.send_ni.Text = "Отправить";
			this.send_ni.UseVisualStyleBackColor = true;
			this.send_ni.Click += new System.EventHandler(this.send_ni_Click);
			// 
			// NewItem
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(354, 559);
			this.Controls.Add(this.send_ni);
			this.Controls.Add(this.ni_rom);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.ni_ram);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.ni_rate_kern);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.ni_mem_v);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.ni_num_kern);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.ni_model_proc);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.ni_model_v);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.ni_name);
			this.Controls.Add(this.label1);
			this.Name = "NewItem";
			this.Text = "Новый элемент";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ni_name;
        private System.Windows.Forms.TextBox ni_model_v;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ni_model_proc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ni_num_kern;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ni_mem_v;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ni_rate_kern;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ni_ram;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ni_rom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button send_ni;
    }
}