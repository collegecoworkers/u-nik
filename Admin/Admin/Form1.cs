﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace Admin
{
    public partial class Form1 : Form
    {
        static private string delimiter = ";;;S";
        static private int DEFAULT_BUFLEN = 8192;

        static private Socket Client;
        private IPAddress ip = null;
        private int port = 7770;

        public Form1()
        {
            InitializeComponent();
       
            try
            {
                var sr = new StreamReader(@"ClientInfo/data_info.txt");
                string buffer = sr.ReadToEnd();
                sr.Close();
                ip = IPAddress.Parse(buffer.Trim());
                SetInfo(Color.Green, "Подключение к " + ip.ToString() + ":" + port.ToString());
            }
            catch (Exception ex)
            {
                SetInfo(Color.Red, "Настройки не найдены");
                ShowSett();
            }
            
            try
            {
                init();
            }
            catch (Exception ex)
            {
                SetInfo(Color.Red, ex.Message);
            }
        }

        void init()
        {
            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (ip != null)
            {
                Client.Connect(ip, port);
                SendMessage("admin");
                SetInfo(Color.Green, "Подключено к " + ip.ToString() + ":" + port.ToString());
            }
        }

        void SetInfo(Color cl, string text){
            info.ForeColor = cl;
            info.Text = text;
        }

        string iw_genNam(string name){
            return name;
        }

        bool iw_has(string name){
            for (int n = listView1.Items.Count - 1; n >= 0; --n)
            {
                if (listView1.Items[n].SubItems[0].Text == iw_genNam(name))
                {
                    return true;
                }
            }
            return false;
        }

        public void iw_add(string[] data){
            ListViewItem lv = new ListViewItem(data[0]);
            lv.SubItems.Add(data[1]);
            lv.SubItems.Add(data[2]);
            lv.SubItems.Add(data[3]);
            lv.SubItems.Add(data[4]);
            lv.SubItems.Add(data[5]);
            lv.SubItems.Add(data[6]);
            lv.SubItems.Add(data[7]);
            listView1.Items.Add(lv);
        }

        public void iw_add(ListViewItem lv){
            listView1.Items.Add(lv);
        }

        int iw_get_id(string name){
           for (int n = listView1.Items.Count - 1; n >= 0; --n)
           {
                if (listView1.Items[n].SubItems[0].Text == iw_genNam(name))
                {
                    return n;
                }
            }
            return -1;
        }

        void iw_rem(string name){
            iw_remAt(iw_get_id(name));
        }

        void iw_remAt(int index){
           listView1.Items.RemoveAt(index);
        }

        void ShowSett()
        {
            Settings s = new Settings();
            s.Show();
        }

        public void SendMessage(string message)
        {
            if (message.Trim() == "" || !Client.Connected)
                return;

            byte[] buffer = new byte[DEFAULT_BUFLEN];
            buffer = Encoding.UTF8.GetBytes(message + delimiter);
            Client.Send(buffer);
        }

        void Log(string mes)
        {
            System.Diagnostics.Debug.WriteLine(mes.ToString());
        }

        private void авторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By Umbokc.\n2018");
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowSett();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BeforeClose();
            Application.Exit();
        }

        private void BeforeClose()
        {
            if (Client != null) Client.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeforeClose();
        }

        private void listBoxItems_SelectedIndexChanged(object sender, EventArgs e){}
        private void listBoxItems_SelectedValueChanged(object sender, EventArgs e){}
        private void label2_Click(object sender, EventArgs e){}
        private void Form1_Load(object sender, EventArgs e){}

        private void add_programm_Click(object sender, EventArgs e)
        {
            (new NewItem(this)).Show();
        }
    }
}
