﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace Client
{
	public partial class Form1 : Form
	{
		static private string delimiter = ";;;S";
		static private int DEFAULT_BUFLEN = 8192;

        static private bool is_disconected = false;
		static private Socket Client;
		private IPAddress ip = null;
		private int port = 7770;
        private Thread th;
        static private int id_selected_item;

		public Form1()
		{
			InitializeComponent();
			
			try
			{
				var sr = new StreamReader(@"ClientInfo/data_info.txt");
				string buffer = sr.ReadToEnd();
				sr.Close();
				ip = IPAddress.Parse(buffer.Trim());
				SetInfo(Color.Blue, "Подключение к " + ip.ToString() + ":" + port.ToString());
			}
			catch (Exception ex)
			{
				SetInfo(Color.Red, "Настройки не найдены");
				ShowSett();
			}
			
			try
			{
				init();
				U.Init();
				SetData();
			}
			catch (Exception ex)
			{
				SetInfo(Color.Red, ex.Message);
			}
		}

		void init()
		{
			Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			if (ip != null)
			{
				Client.Connect(ip, port);
                th = new Thread(delegate () { RecvMessage(); }); th.Start();
				SetInfo(Color.Green, "Подключено к " + ip.ToString() + ":" + port.ToString());
				// test();
			}
		}

        void test()
        {
            string[] data = {
                "ni_name.Text",
                "1050",
                "4096",
                "Intel",
                "4",
                "3.15",
                "8192",
                "4"
            };
            string[] data2 = {
                "Text 2",
                "0",
                "128",
                "Intel",
                "2",
                "2.15",
                "1024",
                "10"
            };
            iw_add(data);
            iw_add(data2);
            listView1.Select();
        }

		void SetInfo(Color cl, string text){
			info.ForeColor = cl;
			info.Text = text;
		}

		void SetData(){
			my_proc.Text = U.get_proc();
			my_videocard.Text = U.get_videocard();
			my_ram.Text = U.get_ram() + " MB";
			my_rom.Text = U.get_rom_free() + " GB";
			my_count_kernel.Text = U.get_count_kernel();
			my_rate.Text = U.get_rate() + " GHz";
			my_mem_v.Text = U.get_mem_v() + " MB";
		}

		string iw_genNam(string name){
			return name;
		}

		string iw_get(int index, int sub_index){
			return listView1.Items[index].SubItems[sub_index].Text;
		}

		bool iw_has(string name){
			for (int n = listView1.Items.Count - 1; n >= 0; --n)
			{
				if (listView1.Items[n].SubItems[0].Text == iw_genNam(name))
				{
					return true;
				}
			}
			return false;
		}

		void iw_add(string[] data){
			ListViewItem lv = new ListViewItem(data[0]);
			lv.SubItems.Add(data[1]);
			lv.SubItems.Add(data[2]);
			lv.SubItems.Add(data[3]);
			lv.SubItems.Add(data[4]);
			lv.SubItems.Add(data[5]);
			lv.SubItems.Add(data[6]);
			lv.SubItems.Add(data[7]);
			iw_add(lv);
		}

		void iw_add(ListViewItem lv){
			listView1.Items.Add(lv);
		}

		int iw_get_id(string name){
			for (int n = listView1.Items.Count - 1; n >= 0; --n)
			{
				if (listView1.Items[n].SubItems[0].Text == iw_genNam(name))
				{
					return n;
				}
			}
			return -1;
		}

		void iw_rem(string name){
			iw_remAt(iw_get_id(name));
		}

		void iw_remAt(int index){
			listView1.Items.RemoveAt(index);
		}

		void ShowSett()
		{
			Settings s = new Settings();
			s.Show();
		}

		public void SendMessage(string message)
		{
			if (message.Trim() == "" || !Client.Connected)
			return;

			byte[] buffer = new byte[DEFAULT_BUFLEN];
			buffer = Encoding.UTF8.GetBytes(message + delimiter);
			Client.Send(buffer);
		}

        void SetDisconected()
        {
			SetInfo(Color.Red, "Disconected");
        }

        void AppendText(string message){
			string[] data = message.Split(',');
			iw_add(data);
        }

        void RecvMessage()
        {
            byte[] buffer = new byte[1024];

            for (int i = 0; i < buffer.Length; i++){ buffer[i] = 0; }

            for (; ; )
            {
                try
                {
                    if (Client.Connected){
                        Client.Receive(buffer);
                        string message = Encoding.UTF8.GetString(buffer);
                        int count = message.IndexOf(delimiter);
                        if (count == -1)
                        {
                            continue;
                        }

                        string clear_message = "";

                        for (int i = 0; i < count; i++) { clear_message += message[i]; }
                        for (int i = 0; i < buffer.Length; i++){ buffer[i] = 0; }

                        this.Invoke((MethodInvoker)delegate ()
                        {
                            AppendText(clear_message);
                        });
                    } else {
                        if(!is_disconected){
                            is_disconected = !is_disconected;
							this.Invoke((MethodInvoker)delegate ()
							{
								SetDisconected();
							});
                            break;
                        }
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }

		private void авторToolStripMenuItem_Click(object sender, EventArgs e)
		{
			MessageBox.Show("Umbokc.\n2018");
		}

		private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ShowSett();
		}

		private void выходToolStripMenuItem_Click(object sender, EventArgs e)
		{
			BeforeClose();
			Application.Exit();
		}

		private void BeforeClose()
		{
            if (th != null) th.Abort();
			if (Client != null) Client.Close();
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			BeforeClose();
		}

		private void label2_Click(object sender, EventArgs e) { }
		private void Form1_Load(object sender, EventArgs e) { }
		private void listBoxItems_SelectedIndexChanged(object sender, EventArgs e){}
		private void listBoxItems_SelectedValueChanged(object sender, EventArgs e){}
		private void label2_Click_1(object sender, EventArgs e){}
		private void listView1_TabIndexChanged(object sender, EventArgs e) { }

		private void add_programm_Click(object sender, EventArgs e){
			// check
			bool yes = true;

			//-----------------------------------

			long model_v = int.Parse(iw_get(id_selected_item, 1));
			long mem_v = long.Parse(iw_get(id_selected_item, 2));
			int num_kernel = int.Parse(iw_get(id_selected_item, 4));
			double rate = double.Parse(iw_get(id_selected_item, 5).Replace('.', ','));
			long ram = long.Parse(iw_get(id_selected_item, 6));
			long rom = long.Parse(iw_get(id_selected_item, 7));

			//check_programm.Enabled = false;
			if(model_v > U.model_v) yes = false;
			if(mem_v > U.mem_v) yes = false;
			if(num_kernel > U.num_kernel) yes = false;
			if(rate > U.rate) yes = false;
			if(ram > U.ram) yes = false;
			if(rom > U.rom) yes = false;

			if(yes){
				MessageBox.Show("Отлично\nВаше оборудование подходит для установки данного ПО");
			} else {
				MessageBox.Show("Нам жаль\nВаше оборудование не подходит для установки данного ПО");
			}
		}

		private void listView1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (listView1.SelectedItems.Count > 0)
			{
				id_selected_item = listView1.Items.IndexOf(listView1.SelectedItems[0]);
				check_programm.Enabled = !false;
				U.Log(id_selected_item);
			} else {
				check_programm.Enabled = false;
			}
		}

	}
}
