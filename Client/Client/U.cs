﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Management;
using System.IO;
using System.Text.RegularExpressions;

namespace Client
{
	public static class U
	{

        public static long model_v = 0;
        public static long mem_v = 0;
        public static int num_kernel = 0;
        public static double rate = 0.0;
        public static long ram = 0;
        public static long rom = 0;

        private static string videocard;

		public static void Init()
		{
			//-------------------------------------------------------
			mem_v = strToMb(GetComponent("Win32_VideoController", "AdapterRam"));
			//-------------------------------------------------------
			videocard = GetComponent("Win32_VideoController", "Name");
			try { model_v = int.Parse(Regex.Match(videocard, @"\d+").Value); }
			catch (Exception ) { model_v = 0; }
			//-------------------------------------------------------
			num_kernel = int.Parse(Environment.GetEnvironmentVariable ( "NUMBER_OF_PROCESSORS"));
			//-------------------------------------------------------
			rate = Math.Round(double.Parse(GetComponent("Win32_processor", "CurrentClockSpeed")) / 1000, 1);
			//-------------------------------------------------------
			ram = strToMb(GetComponent("Win32_ComputerSystem", "TotalPhysicalMemory"));
			//-------------------------------------------------------
			DriveInfo[] allDrives = DriveInfo.GetDrives(); long data = 0;
			foreach (DriveInfo d in allDrives) if (d.IsReady == true) data += d.TotalFreeSpace;
			rom = data / 1_000_000_000;
			//-------------------------------------------------------
		}

		public static void Log(object mes)
		{
			Console.WriteLine(mes.ToString());
		}

		public static long strToMb(string mes)
		{
			return (long) Int64.Parse(mes) / 1_000_000;
		}

		public static string get_videocard()
		{
			return videocard;
		}

		public static string get_mem_v()
		{
			return mem_v.ToString();
		}

		public static string get_proc()
		{
			return GetComponent("Win32_Processor", "Name");
		}

		public static string get_count_kernel()
		{
			return num_kernel.ToString();
		}

		public static string get_rate()
		{
			return rate.ToString();
		}

		public static string get_ram()
		{
			return ram.ToString();
		}

		public static string get_rom_free()
		{
			return rom.ToString();
		}

		static string GetComponent(string hwclass, string syntax)
		{
			string data = "";

			ManagementObjectSearcher mos = GetComponentData(hwclass);

			foreach (ManagementObject mo in mos.Get())
			{
				data += Convert.ToString(mo[syntax]);
			}

			return data;
		}

		static ManagementObjectSearcher GetComponentData(string hwclass)
		{
			return new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM " + hwclass);
		}
	}
}
