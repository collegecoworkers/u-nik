﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.менюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.авторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.groupBoxMain = new System.Windows.Forms.GroupBox();
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.info = new System.Windows.Forms.Label();
			this.check_programm = new System.Windows.Forms.Button();
			this.asdsadas = new System.Windows.Forms.Label();
			this.my_proc = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.my_videocard = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.my_ram = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.my_rom = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.my_count_kernel = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.my_rate = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.my_mem_v = new System.Windows.Forms.Label();
			this.menuStrip1.SuspendLayout();
			this.groupBoxMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.менюToolStripMenuItem,
            this.авторToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1050, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// менюToolStripMenuItem
			// 
			this.менюToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.настройкиToolStripMenuItem,
            this.выходToolStripMenuItem});
			this.менюToolStripMenuItem.Name = "менюToolStripMenuItem";
			this.менюToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
			this.менюToolStripMenuItem.Text = "Меню";
			// 
			// настройкиToolStripMenuItem
			// 
			this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
			this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
			this.настройкиToolStripMenuItem.Text = "Настройки";
			this.настройкиToolStripMenuItem.Click += new System.EventHandler(this.настройкиToolStripMenuItem_Click);
			// 
			// выходToolStripMenuItem
			// 
			this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
			this.выходToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
			this.выходToolStripMenuItem.Text = "Выход";
			this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
			// 
			// авторToolStripMenuItem
			// 
			this.авторToolStripMenuItem.Name = "авторToolStripMenuItem";
			this.авторToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
			this.авторToolStripMenuItem.Text = "Автор";
			this.авторToolStripMenuItem.Click += new System.EventHandler(this.авторToolStripMenuItem_Click);
			// 
			// groupBoxMain
			// 
			this.groupBoxMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBoxMain.Controls.Add(this.listView1);
			this.groupBoxMain.Location = new System.Drawing.Point(12, 82);
			this.groupBoxMain.Name = "groupBoxMain";
			this.groupBoxMain.Size = new System.Drawing.Size(775, 329);
			this.groupBoxMain.TabIndex = 1;
			this.groupBoxMain.TabStop = false;
			this.groupBoxMain.Text = "Программы/Игры";
			// 
			// listView1
			// 
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
			this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listView1.Location = new System.Drawing.Point(3, 16);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(769, 310);
			this.listView1.TabIndex = 0;
			this.listView1.UseCompatibleStateImageBehavior = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			this.listView1.TabIndexChanged += new System.EventHandler(this.listView1_TabIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Название";
			this.columnHeader1.Width = 134;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Модель видеокарты";
			this.columnHeader2.Width = 121;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Память видеокарты";
			this.columnHeader3.Width = 121;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Модель процессора";
			this.columnHeader4.Width = 116;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Кол-во ядер";
			this.columnHeader5.Width = 82;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "Частота ядра";
			this.columnHeader6.Width = 88;
			// 
			// columnHeader7
			// 
			this.columnHeader7.Text = "ОЗУ";
			this.columnHeader7.Width = 50;
			// 
			// columnHeader8
			// 
			this.columnHeader8.Text = "ЖД";
			// 
			// info
			// 
			this.info.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.info.AutoSize = true;
			this.info.Location = new System.Drawing.Point(14, 43);
			this.info.Name = "info";
			this.info.Size = new System.Drawing.Size(111, 13);
			this.info.TabIndex = 2;
			this.info.Text = "Статус подключения";
			this.info.Click += new System.EventHandler(this.label2_Click);
			// 
			// check_programm
			// 
			this.check_programm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.check_programm.Enabled = false;
			this.check_programm.Location = new System.Drawing.Point(14, 417);
			this.check_programm.Name = "check_programm";
			this.check_programm.Size = new System.Drawing.Size(178, 35);
			this.check_programm.TabIndex = 3;
			this.check_programm.Text = "Проверить на совместимость";
			this.check_programm.UseVisualStyleBackColor = true;
			this.check_programm.Click += new System.EventHandler(this.add_programm_Click);
			// 
			// asdsadas
			// 
			this.asdsadas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.asdsadas.AutoSize = true;
			this.asdsadas.Location = new System.Drawing.Point(793, 98);
			this.asdsadas.Name = "asdsadas";
			this.asdsadas.Size = new System.Drawing.Size(121, 13);
			this.asdsadas.TabIndex = 4;
			this.asdsadas.Text = " Ваши характеристики";
			// 
			// my_proc
			// 
			this.my_proc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.my_proc.AutoSize = true;
			this.my_proc.Location = new System.Drawing.Point(801, 138);
			this.my_proc.Name = "my_proc";
			this.my_proc.Size = new System.Drawing.Size(121, 13);
			this.my_proc.TabIndex = 5;
			this.my_proc.Text = " Ваши характеристики";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(795, 125);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(76, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "Процессор:";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(795, 161);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(77, 13);
			this.label2.TabIndex = 8;
			this.label2.Text = "Видеокарта";
			this.label2.Click += new System.EventHandler(this.label2_Click_1);
			// 
			// my_videocard
			// 
			this.my_videocard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.my_videocard.AutoSize = true;
			this.my_videocard.Location = new System.Drawing.Point(801, 174);
			this.my_videocard.Name = "my_videocard";
			this.my_videocard.Size = new System.Drawing.Size(121, 13);
			this.my_videocard.TabIndex = 7;
			this.my_videocard.Text = " Ваши характеристики";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(795, 231);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(132, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "Оперативная память";
			// 
			// my_ram
			// 
			this.my_ram.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.my_ram.AutoSize = true;
			this.my_ram.Location = new System.Drawing.Point(801, 244);
			this.my_ram.Name = "my_ram";
			this.my_ram.Size = new System.Drawing.Size(121, 13);
			this.my_ram.TabIndex = 9;
			this.my_ram.Text = " Ваши характеристики";
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(795, 265);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(144, 13);
			this.label4.TabIndex = 12;
			this.label4.Text = "Свободная память ЖД";
			// 
			// my_rom
			// 
			this.my_rom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.my_rom.AutoSize = true;
			this.my_rom.Location = new System.Drawing.Point(801, 278);
			this.my_rom.Name = "my_rom";
			this.my_rom.Size = new System.Drawing.Size(121, 13);
			this.my_rom.TabIndex = 11;
			this.my_rom.Text = " Ваши характеристики";
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(795, 301);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(108, 13);
			this.label5.TabIndex = 14;
			this.label5.Text = "Количество ядер";
			// 
			// my_count_kernel
			// 
			this.my_count_kernel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.my_count_kernel.AutoSize = true;
			this.my_count_kernel.Location = new System.Drawing.Point(801, 314);
			this.my_count_kernel.Name = "my_count_kernel";
			this.my_count_kernel.Size = new System.Drawing.Size(121, 13);
			this.my_count_kernel.TabIndex = 13;
			this.my_count_kernel.Text = " Ваши характеристики";
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label6.Location = new System.Drawing.Point(793, 337);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(130, 13);
			this.label6.TabIndex = 16;
			this.label6.Text = "Частота процессора";
			// 
			// my_rate
			// 
			this.my_rate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.my_rate.AutoSize = true;
			this.my_rate.Location = new System.Drawing.Point(799, 350);
			this.my_rate.Name = "my_rate";
			this.my_rate.Size = new System.Drawing.Size(121, 13);
			this.my_rate.TabIndex = 15;
			this.my_rate.Text = " Ваши характеристики";
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label7.Location = new System.Drawing.Point(795, 194);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(86, 13);
			this.label7.TabIndex = 18;
			this.label7.Text = "Видеопамять";
			// 
			// my_mem_v
			// 
			this.my_mem_v.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.my_mem_v.AutoSize = true;
			this.my_mem_v.Location = new System.Drawing.Point(801, 207);
			this.my_mem_v.Name = "my_mem_v";
			this.my_mem_v.Size = new System.Drawing.Size(121, 13);
			this.my_mem_v.TabIndex = 17;
			this.my_mem_v.Text = " Ваши характеристики";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1050, 465);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.my_mem_v);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.my_rate);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.my_count_kernel);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.my_rom);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.my_ram);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.my_videocard);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.my_proc);
			this.Controls.Add(this.asdsadas);
			this.Controls.Add(this.check_programm);
			this.Controls.Add(this.info);
			this.Controls.Add(this.groupBoxMain);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.MinimumSize = new System.Drawing.Size(573, 503);
			this.Name = "Form1";
			this.Text = "Система анализа соответствия технических средств";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.groupBoxMain.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem менюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem авторToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxMain;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Button check_programm;
        private System.Windows.Forms.Label asdsadas;
		private System.Windows.Forms.Label my_proc;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label my_videocard;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label my_ram;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label my_rom;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label my_count_kernel;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label my_rate;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label my_mem_v;
	}
}

