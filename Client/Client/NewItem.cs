﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class NewItem : Form
    {
        Form1 form;

        public NewItem(Form1 f1)
        {
            form = f1;
            InitializeComponent();
        }

        private void send_ni_Click(object sender, EventArgs e)
        {
            if(ni_name.Text.Trim() != "" && ni_model_v.Text.Trim() != "" && ni_mem_v.Text.Trim() != "" && ni_model_proc.Text.Trim() != "" && ni_num_kern.Text.Trim() != "" && ni_rate_kern.Text.Trim() != "" && ni_ram.Text.Trim() != "" && ni_rom.Text.Trim() != "")
            {
                string[] data = {ni_name.Text, ni_model_v.Text, ni_mem_v.Text, ni_model_proc.Text, ni_num_kern.Text, ni_rate_kern.Text, ni_ram.Text, ni_rom.Text};
                form.iw_add(data);
                form.SendMessage(ni_name.Text +","+ ni_model_v.Text +","+ ni_mem_v.Text +","+ ni_model_proc.Text +","+ ni_num_kern.Text +","+ ni_rate_kern.Text +","+ ni_ram.Text +","+ ni_rom.Text);
                this.Close();
            }
        }
    }
}
